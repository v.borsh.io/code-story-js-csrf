# CSRF Story

**To Read**

[Cross Site Request Forgery (CSRF)](https://owasp.org/www-community/attacks/csrf)
[CSRF Prevention](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html)

## Story Outline

Following story describe commonly faced security issue CSRF and how we can deal with it in Node.js context 
