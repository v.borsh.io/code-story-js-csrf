const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require('cookie-parser');
const csurf = require('csurf');

const PORT = process.env.PORT || 3000;
const App = express();

const mockUser = {
  uname: "alberto",
  pass: "password",
  sessionId: null,
}

App.use(bodyParser.urlencoded({ extended: true }));
App.use(cookieParser());
App.use(csurf({ cookie: true }));

App.use((err, _, response, next) => {
  if (err.code !== 'EBADCSRFTOKEN') {
    return next(err);
  }
  response.status(403);
  response.send('form is broken');
})

App.get("/", (request, response) => {
  response.send(`
    <h3>Auth</h3>
    <form action="/auth" method="POST">
      <div>
        <label>Enter username</label>
        <input name="uname" type="text">
      </div>
      <div>
        <label for="pass">Enter password</label>
        <input name="pass" type="password">
      </div>
      <input type="hidden" name="_csrf" value="${request.csrfToken()}" />
      <button type="submit">Log in</button>
    </form>
  `);
});

App.get("/transfer", (request, response) => {
  response.send(`
    <h2>Transfer form</h2>
    <h4>Your account was successfully recognized</h4>
    <p>Now you are ready to transfer money. Please make sure you typed correct destination account number</p>
    <form action="/checkout" method="POST">
      <div>
        <label>Account number (for who you want send money)</label><br>
        <input name="to_account" type="text">
      </div>
      <div>
        <label>USD Amount</label><br>
        <input name="usd_amount" type="number">
      </div>
      <br>
      <input type="hidden" name="_csrf" value="${request.csrfToken()}" />
      <button type="submit">Send</button>
    </form>
  `);
});

App.post('/auth', (request, response) => {
  if (mockUser.uname === request.body.uname && mockUser.pass === request.body.pass) {
    mockUser.sessionId = Math.random().toString();
    response.cookie('SID', mockUser.sessionId, { maxAge: 3600000 });
    response.redirect('/transfer');
  } else {
    response.send(`Incorrect credentials`);
  }
});

App.post('/checkout', (request, response) => {
  if(request.cookies['SID'] === mockUser.sessionId) {
    response.send(`
      <h4>Transfer succeed:</h4>
      ${request.body.usd_amount} USD was sent to account ${request.body.to_account}
    `);
  } else {
    response.send(`Access forbidden: invalid session id`)
  }
});

App.listen(PORT, () => console.log(`App is running on port ${PORT}`));